# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = User.create([ 
						{:first_name => "maverick", :last_name => "malficent"},
                        {:first_name => "John", :last_name => "Paul John"},
                        {:first_name => "nick", :last_name => "saverin"},
                        {:first_name => "zooey", :last_name => "deschannel"},
                        {:first_name => "emily", :last_name => "browning"},
                    ]) 

articles = Article.create ([
	{:title => "judul1", :body => "content1"},
	{:title => "judul2", :body => "content2"},
	{:title => "judul3", :body => "content3"},
	{:title => "judul4", :body => "content4"},
	{:title => "judul5", :body => "content5"},
	])

comments = Comment.create ([
	{:content => "comment1"},
	{:content => "comment2"},
	{:content => "comment3"},
	{:content => "comment4"},
	{:content => "comment5"},
	])

countries = Country.create ([
	{:code => "INA", :name => "Indonesia"},
	{:code => "ITA", :name => "Italy"},
	{:code => "NED", :name => "Netherland"},
	{:code => "FRA", :name => "France"},
	{:code => "UKR", :name => "Ukraine"},
	])