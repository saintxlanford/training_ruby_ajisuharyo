class AddCountryIdOnUser < ActiveRecord::Migration
  def up
  	add_column :users, :country_id, :string
  end

  def down
  end
end
