class RenameColumnArticles < ActiveRecord::Migration
  def up
  	rename_column :articles, :body, :description
  end

  def down
  end
end
