class AddUserIdOnCategory < ActiveRecord::Migration
  def up
  	add_column :categories, :user_id, :integer
  end

  def down
  end
end
