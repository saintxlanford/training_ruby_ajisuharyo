class AddArticleIdOnComment < ActiveRecord::Migration
  def up
  	add_column :comments, :article_id, :string
  end

  def down
  end
end
