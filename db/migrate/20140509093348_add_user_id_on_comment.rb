class AddUserIdOnComment < ActiveRecord::Migration
  def up
  	add_column :comments, :user_id, :string
  end

  def down
  end
end
