class AddUserIdOnArticle < ActiveRecord::Migration
  def up
  	add_column :articles, :user_id, :string
  end

  def down
  end
end
