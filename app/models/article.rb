class Article < ActiveRecord::Base 
  attr_accessible :title, :description, :rating, :user_id

  has_many :comments, :dependent => :destroy
  belongs_to :user

  scope :rating, where("rating > ?", 0)

   def self.count_content
   	where("length(description) > 100")
  end

  validate :valid_title
  validates :title, 
    		:presence => { :message => "can't be blank, must be filled" },
            :uniqueness => true,
            :allow_nil => false,
            :allow_blank => false

  validates :description,
    		:presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false
            
  validates :user_id,
    		:presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false

  validates :rating,
    		:presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false

  def valid_title
   		self.errors[:title] << "can't left blank" if (title == 'nill' || title == 'empty' || title == 'blank')
 	end
end
