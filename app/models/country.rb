class Country < ActiveRecord::Base
  attr_accessible :code, :name

  has_many :users

  has_many :people,
           		:class_name => "User" ,
             	:foreign_key => "country_id" 

  validate :valid_country
  validates :code,
    		:presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false

  validates :name,
    		:presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false

  def valid_country
   		self.errors[:code] << "Just can be filled by 'id', 'usa', and 'chn'" unless (code == 'id' || code == 'usa' || code == 'chn')
 	end
end