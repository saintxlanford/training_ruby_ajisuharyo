class Comment < ActiveRecord::Base
  attr_accessible :content, :user_id, :article_id

  belongs_to :user
  belongs_to :article

  validates :content,
    		:presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false

  validates :article_id,
    		:presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false

  validates :user_id,
    		:presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false
end