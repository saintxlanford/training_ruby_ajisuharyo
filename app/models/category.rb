class Category < ActiveRecord::Base
  attr_accessible :name, :user_id

  has_and_belongs_to_many :products

  #has_many :categories_products
  #has_many :products, :through => :categories_products

  scope :books, where("name like '%book%'")

  has_and_belongs_to_many :shoes,
           		:class_name => "Product" ,       		
             	:foreign_key => "category_id" ,
             	:conditions => "name like '%shoes%'"

  validates :name,
    		:presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false

  validates :user_id,
        :presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false
end
