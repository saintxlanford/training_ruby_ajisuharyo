class CategoriesProduct < ActiveRecord::Base
  attr_accessible :product_id, :category_id

  belongs_to :category
  belongs_to :product

  validates :category_id,
    		:presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false

  validates :product_id,
    		:presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false
end
