class Product < ActiveRecord::Base 
  attr_accessible :name, :price, :stock, :description, :user_id

  belongs_to :user

  has_many :categories_products
  has_many :categories, :through => :categories_products

  #scope :stock, where("stock > ?", 0)
  scope :stock, lambda { |stock| where('stock = ?' , stock) }
  #scope :price_less_than, where("price <= ?", 1000)
  scope :price_less_than, lambda { |price| where('price < ?' , price) }

  validates :price, 
    		:presence => { :message => "can't be blank, must be filled" },
            :format => {:with => /^[0-9]*$/}

  validates :description,
    		:presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false

  validates :name,
    		:presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false

  validates :stock,
    		:presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false

  validates :user_id,
    		:presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false
  
end
