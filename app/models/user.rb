class User < ActiveRecord::Base
  attr_accessible :first_name, :last_name, :email, :username, :password, :password_confirmation, :date_of_birth, :age, :address, :country_id
  attr_accessor :password
  before_save :encrypt_password

  has_many :products
  has_many :articles
  has_many :comments
  belongs_to	:country

  has_many :creator,
              :class_name => "Article" ,
              :foreign_key => "user_id" ,
              :conditions => "title like '%my country%'"

  #scope :nation, where("country_id = 1")
  scope :indonesia, where("countries.name LIKE '%indonesia%'").includes(:country)

  def show_full_address
  	country = Country.find(self.country_id)
    "#{self.first_name} #{self.last_name} #{self.address} #{country.name}"
  end

  def self.authenticate(email, password)
      user = find_by_email(email)
      if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
        user
      else
        nil
      end
  end

  def encrypt_password
      if password.present?
          self.password_salt = BCrypt::Engine.generate_salt
          self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
      end
  end

  def self.min_age
   	where("age > 18")
  end

    validates :email, 
    		:presence => { :message => "can't be blank, must be filled" },
            :length => {:minimum => 1, :maximum => 25},
            :uniqueness => true,
            :allow_nil => false,
            :format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}

    validates :first_name, :last_name, 
    		:presence => { :message => "can't be blank, must be filled" },
            :length => {:minimum => 1, :maximum => 25},
            :uniqueness => true,
            :allow_nil => false,
            :format => { :with => /\A[a-zA-Z]+\z/,
    			:message => "Only letters allowed" }

    validates :username,
        :presence => { :message => "can't be blank, must be filled" },
           :allow_nil => false,
            :allow_blank => false

    validates :password, :presence => {:on => :create},
                     :confirmation => true

    validates :date_of_birth,
        :presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false

    validates :age,
        :presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false

    validates :address,
        :presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false

    validates :country_id,
        :presence => { :message => "can't be blank, must be filled" },
            :allow_nil => false,
            :allow_blank => false

end
