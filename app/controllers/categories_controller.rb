class CategoriesController < ApplicationController
  before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]

	def index
    	@categories = Category.all
  	end

    def show
        @categories = Category.find(params[:id])
    end

	  def new
    	@categories = Category.new
  	end

  	def create
      	@categories = Category.new(params[:category])
        #raise params.inspect
        if verify_recaptcha
      	if @categories.save
           flash[:notice] = 'Category was successfully created.'
            redirect_to :action => 'index'
     	 else
            flash[:error] = 'Category was failed to create.'
            render :action => 'new'
      	end
        else
        flash[:error] = "There was an error with the recaptcha code below. Please re-enter the code and click submit."
        @countries = Country.all
        render :action => 'new'
      end
   	end

  	def edit
      	@categories = Category.find(params[:id])
   	end	
  	
  	def update
      	@categories = Category.find(params[:id])
      	if @categories.update_attributes(params[:category])
           flash[:notice] = 'Category was successfully updated.'
         	redirect_to :action => 'index'
      	else
          flash[:error] = 'Category was failed to update.'
         	render :action => 'edit'
      	end
   	end

  	def destroy
      @categories = Category.find(params[:id])
      @categories.destroy
      redirect_to :action => 'index'
   end

end
