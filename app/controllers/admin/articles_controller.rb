class Admin::ArticlesController < ApplicationController
	before_filter :require_admin_login

	def index
    	@articles = Article.all
  	end

  	def show
      	@articles = Article.find(params[:id])
      	@by = @articles.user_id
      	@user = User.find_by_id(@by)
        @comments = Comment.find_all_by_article_id(params[:id])
        @comment  = Comment.new
   	end

	  def new
    	@articles = Article.new
  	end

  	def create
      	@articles = Article.new(params[:article])
      	if @articles.save
           flash[:notice] = 'Article was successfully created.'
            redirect_to :action => 'index'
     	 else
            flash[:error] = 'Article was failed to create.'
            render :action => 'new'
      	end
   	end

  	def edit
      	@article = Article.find(params[:id])
   	end	
  	
  	def update
      	@article = Article.find(params[:id])
      	if @article.update_attributes(params[:article])
           flash[:notice] = 'Article was successfully updated.'
         	redirect_to :action => 'show', :id => @article
      	else
          flash[:error] = 'Article was failed to update.'
         	render :action => 'edit'
      	end
   	end

  	def destroy
      @article = Article.find(params[:id])
        @article.destroy
        redirect_to :action => 'index'
    end
end
