class CountriesController < ApplicationController
  before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]
  
	def index
    	@countries = Country.all
  	end

  	def show
      	@countries = Country.find(params[:id])
   	end

	  def new
    	@countries = Country.new
  	end

  	def create
      	@countries = Country.new(params[:country])
      	if @countries.save
            flash[:notice] = 'Country was successfully created.'
            redirect_to :action => 'index'
     	 else
            flash[:error] = 'Country was failed to create.'
            render :action => 'new'
      	end
   	end

  	def edit
      	@countries = Country.find(params[:id])
   	end	
  	
  	def update
      	@countries = Country.find(params[:id])
      	if @countries.update_attributes(params[:country])
          flash[:notice] = 'Country was successfully updated.'
         	redirect_to :action => 'show', :id => @countries
      	else
          flash[:error] = 'Country was failed to update.'
         	render :action => 'edit'
      	end
   	end

  	def destroy
      @countries = Country.find(params[:id])
      @countries.destroy
      redirect_to :action => 'index'
   end

end
