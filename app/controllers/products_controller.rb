class ProductsController < ApplicationController
  before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]

	def index
    	@products = Product.all
  	end

  	def show
      	@products = Product.find(params[:id])
        @by = @products.user_id
        @user = User.find_by_id(@by)
   	end

	  def new
    	@products = Product.new
  	end

  	def create
      	@products = Product.new(params[:product])
      	if @products.save
           flash[:notice] = 'Product was successfully created.'
            redirect_to :action => 'index'
     	 else
            flash[:error] = 'Product was failed to create.'
            render :action => 'new'
      	end
   	end

  	def edit 
      	@product = Product.find(params[:id])
   	end	
  	
  	def update
      	@product = Product.find(params[:id])
      	if @product.update_attributes(params[:product])
           flash[:notice] = 'Product was successfully updated.'
         	redirect_to :action => 'show', :id => @product
      	else
          flash[:error] = 'Product was failed to update.'
         	render :action => 'edit'
      	end
   	end

  	def destroy
      @products = Product.find(params[:id])
      @products.destroy
      redirect_to :action => 'index'
   end

end
