class UsersController < ApplicationController
  #before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]

	def index
    	@users = User.all
  	end

  	def show
      	@user = User.find(params[:id])
      	@country = @user.country_id
      	@user_country = Country.find_by_id(@country)
   	end

	  def new
    	@user = User.new
    	@countries = Country.all
  	end

  	def create
      @user = User.new(params[:user])
     # raise params.inspect
      if verify_recaptcha
      	   if @user.save
              flash[:notice] = 'User was successfully created.'
#             redirect_to :action => 'index'
              UserMailer.registration_confirmation(@user).deliver
              redirect_to root_url, :notice => "Signed up!" 
       	   else
              flash[:error] = 'User was failed to create.'
              @countries = Country.all
              render :action => 'new'
      	   end
      else
        flash[:error] = "There was an error with the recaptcha code below. Please re-enter the code and click submit."
        @countries = Country.all
        render :action => 'new'
      end
   	end

  	def edit
      	@user = User.find(params[:id])
      	@countries = Country.all
   	end	
  	
  	def update
      	@user = User.find(params[:id])
      	if @user.update_attributes(params[:user])
           flash[:notice] = 'User was successfully updated.'
         	redirect_to :action => 'show', :id => @user
      	else
          flash[:error] = 'User was failed to update.'
          @countries = Country.all
         	render :action => 'edit'
      	end
   	end

  	def destroy
      @user = User.find(params[:id])
      @user.destroy
      redirect_to :action => 'index'
   end

end
