class ArticlesController < ApplicationController
  before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]

	  def index
    	@articles = Article.find_all_by_user_id(session[:user_id])
  	end

  	def show
      	@articles = Article.find(params[:id])
      	@by = @articles.user_id
      	@user = User.find_by_id(@by)
        @comments = Comment.find_all_by_article_id(params[:id])
        @comment  = Comment.new
   	end

	  def new
    	@articles = Article.new
  	end

  	def create
      	@articles = Article.new(params[:article])
      	if @articles.save
           flash[:notice] = 'Article was successfully created.'
            redirect_to :action => 'index'
     	 else
            flash[:error] = 'Article was failed to create.'
            render :action => 'new'
      	end
   	end

  	def edit
      	@article = Article.find(params[:id])
   	end	
  	
  	def update
      	@article = Article.find(params[:id])
      	if @article.update_attributes(params[:article])
           flash[:notice] = 'Article was successfully updated.'
         	redirect_to :action => 'show', :id => @article
      	else
          flash[:error] = 'Article was failed to update.'
         	render :action => 'edit'
      	end
   	end

  	def destroy
      @article = Article.find(params[:id])
      if @article.user_id == session[:user_id]
        @article.destroy
        redirect_to :action => 'index'
      else
        flash[:notice] = "Article can't be deleted"
        redirect_to :action => 'index'
    end
   end

end
