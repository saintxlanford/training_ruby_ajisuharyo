require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def test_save_without_title
  	article = Article.new(:description=>'new_desc',:rating=>'1',:user_id=>'2')
  	assert_equal article.valid?, false
  	assert_equal article.save, false
  end

  def test_save_without_description
  	article = Article.new(:title=>'new_title',:rating=>'1',:user_id=>'2')
  	assert_equal article.valid?, false
  	assert_equal article.save, false
  end
  def test_save_without_rating
  	article = Article.new(:title=>'new_title',:description=>'new_desc',:user_id=>'2')
  	assert_equal article.valid?, false
  	assert_equal article.save, false
  end
  def test_save_without_user_id
  	article = Article.new(:title=>'new_title',:description=>'new_desc',:rating=>'1')
  	assert_equal article.valid?, false
  	assert_equal article.save, false
  end
  def test_save_with_title_description_rating_and_user_id
    article = Article.new(:title=>'new_title',:description=>'new_desc',:rating=>'1',:user_id=>'2')
    assert_equal article.valid?, true
    assert_equal article.save, true
  end

  def test_find_top_article
    Article.create([
      {:title=>'new_title super',:description=>'new_desc',:rating=>65,:user_id=>2}, {:title=>'new_title2',:description=>'new_desc2',:rating=>'2',:user_id=>'1'},
    ])
    assert_not_nil Article.rating(65)
    assert_equal Article.rating(65)[0].rating, 65
  end
  def test_find_long_article
    Article.create([
      {:title=>'new_title super',:description=>'new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc new_desc ',:rating=>65,:user_id=>2}, {:title=>'new_title2',:description=>'new_desc2',:rating=>'2',:user_id=>'1'},
    ])
    assert_not_nil Article.count_content
  end

  def test_relation_between_article_and_comment
    article = Article.create(:title => "new_title", :description => "new content",:rating=>"30",:user_id=>"3")
    assert_not_nil article
    comment = Comment.create(:article_id => article.id, :content => "my comment",:user_id=>"4")
    assert_not_nil article.comments
    assert_equal article.comments.empty?, false
    assert_equal article.comments[0].class, Comment
  end

end
