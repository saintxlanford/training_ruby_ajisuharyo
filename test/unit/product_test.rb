require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def test_save_without_name
  	product = Product.new(:description=>'new_desc',:price=>'10000',:stock=>'23',:user_id=>'2')
  	assert_equal product.valid?, false
  	assert_equal product.save, false
  end
  def test_save_without_description
  	product = Product.new(:name=>'new_name',:price=>'10000',:stock=>'23',:user_id=>'2')
  	assert_equal product.valid?, false
  	assert_equal product.save, false
  end
  def test_save_without_price
  	product = Product.new(:name=>'new_name',:description=>'new_desc',:stock=>'23',:user_id=>'2')
  	assert_equal product.valid?, false
  	assert_equal product.save, false
  end
  def test_save_without_stock
  	product = Product.new(:name=>'new_name',:description=>'new_desc',:price=>'10000',:user_id=>'2')
  	assert_equal product.valid?, false
  	assert_equal product.save, false
  end
  def test_save_without_user_id
  	product = Product.new(:name=>'new_name',:description=>'new_desc',:price=>'10000',:stock=>'23')
  	assert_equal product.valid?, false
  	assert_equal product.save, false
  end
  def test_save_with_name_description_price_stock_and_user_id
    product = Product.new(:name=>'new_name',:description=>'new_desc',:price=>'10000',:stock=>'23',:user_id=>'2')
    assert_equal product.valid?, true
    assert_equal product.save, true
  end
  def test_find_product_by_stock
    Product.create([
      {:name=>'new_name',:description=>'new_desc2',:price=>'15000',:stock=>'123',:user_id=>'1'}, {:name=>'new_name2',:description=>'new_desc',:price=>'10000',:stock=>'23',:user_id=>'2'},
    ])
    assert_not_nil Product.stock(123)
    assert_equal Product.stock(123)[0].stock, '123'
  end
  def test_find_product_by_price
    Product.create([
      {:name=>'new_name',:description=>'new_desc2',:price=>'15000',:stock=>'123',:user_id=>'1'}, {:name=>'new_name2',:description=>'new_desc',:price=>'10000',:stock=>'23',:user_id=>'2'},
    ])
    assert_not_nil Product.price_less_than(20000)
    assert_equal Product.price_less_than(20000)[0].price, '15000'
    #assert_not_nil Product.price(20000)
    #assert_equal Product.price(20000)[0].price, 15000
  end
  def test_relation_between_product_and_category
    category = Category.create(:name=>'new_cat',:user_id=>'2')
    assert_not_nil category
    product = Product.create(:name => 'new_prod', :price => "20000",:stock=>32,:description=>"new_desc",:user_id=>"2")
    assert_not_nil product
    cat_prod = CategoriesProduct.create(:category_id=>category.id,:product_id=>product.id)
    assert_not_nil category.products
    assert_equal category.products.empty?, false
    assert_equal category.products[0].class, Product
  end
end
