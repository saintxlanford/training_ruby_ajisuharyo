require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def test_save_without_content
    comment = Comment.new(:user_id=>1,:article_id=>3)
    assert_equal comment.valid?, false
    assert_equal comment.save, false
  end
  def test_save_without_user_id
    comment = Comment.new(:content=>'new_content',:article_id=>2)
    assert_equal comment.valid?, false
    assert_equal comment.save, false
  end
  def test_save_without_article_id
    comment = Comment.new(:content=>'new_content',:user_id=>4)
    assert_equal comment.valid?, false
    assert_equal comment.save, false
  end
  def test_save_with_name_user_id_and_article_id
    comment = Comment.new(:content=>'new_name',:user_id=>'1',:article_id=>'2')
    assert_equal comment.valid?, true
    assert_equal comment.save, true
  end
end
